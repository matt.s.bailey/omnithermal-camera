#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "LeptonThread.h"

#include <QMainWindow>
#include <QImage>
#include <QLabel>
#include <QPushButton>
#include <QGridLayout>
#include <QImage>
#include <QPixmap>
#include <QPainter>
#include <QFile>
#include <QDebug>

#include <ros/ros.h>
#include <std_msgs/UInt16MultiArray.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

//class LeptonThread;

class ThermalPublisher : public QObject {
Q_OBJECT
private:

    ros::NodeHandle nh;
    image_transport::Publisher pub;
    unsigned short rawMin, rawMax;
    QVector<unsigned short> rawData;

public:

	explicit ThermalPublisher(QObject *parent = nullptr);

public slots:
    void updateImage(unsigned short *, int, int);
};

#endif // MAINWINDOW_H

#include "LeptonThread.h"

#include <QString>
#include <QTextStream>
#include <QtSerialPort/QSerialPort>
LeptonThread::LeptonThread()
    : QThread()
    , result(RowPacketBytes*FrameHeight)
    , rawData(FrameWords) { }

LeptonThread::~LeptonThread() { }

void LeptonThread::run() {
    while(true){
		while(!serialPortReader0.ready || !serialPortReader1.ready || !serialPortReader2.ready || !serialPortReader3.ready){}
		standardOutput << QObject::tr("DATA: ") << serialPortReader0.m_readData[0] << QObject::tr(" ") << serialPortReader0.ready  << endl;

		for(int i = 0; i < FrameHeight; i++){
			for(int j=0; j<FrameWidth; j++){
				data.append(serialPortReader0.m_readData[i*FrameWidth+j]);
			}
			for(int j=0; j<FrameWidth; j++){
				data.append(serialPortReader1.m_readData[i*FrameWidth+j]);
			}
			for(int j=0; j<FrameWidth; j++){
				data.append(serialPortReader2.m_readData[i*FrameWidth+j]);
			}
			for(int j=0; j<FrameWidth; j++){
				data.append(serialPortReader3.m_readData[i*FrameWidth+j]);
			}
		}
		
        uint16_t minValue = 65535;
        uint16_t maxValue = 0;
        char *in = data.data();
        unsigned short *out = &rawData[0];
        for (int iRow = 0; iRow < FrameHeight; ++iRow) {
            for (int iCol = 0; iCol < FrameWidth; ++iCol) {
                unsigned short value = in[0];
                value <<= 8;
                value |= in[1];
                in += 2;
                if (value > maxValue) maxValue = value;
                if (value < minValue) minValue = value;
                *(out++) = value;
            }
        }
		emit updateImage(&rawData[0], minValue, maxValue);
		serialPortReader0.ready = false;
		serialPortReader0.m_readData.clear();
		serialPortReader1.ready = false;
		serialPortReader1.m_readData.clear();
		serialPortReader2.ready = false;
		serialPortReader2.m_readData.clear();
		serialPortReader3.ready = false;
		serialPortReader3.m_readData.clear();
		for (int i = 0; i< NUM_SERIAL_PORTS; i++){
			serialPort[i]->write("a");
		}
    }
}

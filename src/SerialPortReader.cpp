/****************************************************************************
**
** Copyright (C) 2013 Laszlo Papp <lpapp@kde.org>
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtSerialPort module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "SerialPortReader.h"
#include <QApplication>
#include <QtMath>
QT_USE_NAMESPACE

SerialPortReader::SerialPortReader(QSerialPort *serialPort, QObject *parent)
: QObject(parent)
, m_serialPort(serialPort)
, m_standardOutput(stdout)
{
    connect(m_serialPort, &QSerialPort::readyRead, this, &SerialPortReader::handleReadyRead);
    connect(m_serialPort, static_cast<void (QSerialPort::*)(QSerialPort::SerialPortError)>(&QSerialPort::error),
        this, &SerialPortReader::handleError);
    connect(&m_timer, &QTimer::timeout, this, &SerialPortReader::handleTimeout);
    ready = false;
    m_timer.start(TimeoutValue);
}

SerialPortReader::~SerialPortReader()
{
}

void SerialPortReader::handleReadyRead()
{

 m_readData.append(m_serialPort->readAll());
 if (!m_timer.isActive())
    m_timer.start(TimeoutValue);

}

void SerialPortReader::handleTimeout()
{
    //m_standardOutput << QObject::tr("FIRST SIZE: %1\n").arg(m_readData.size());
    if (m_readData.size() == (FrameHeight*FrameWidth*2)) {
        //for(int i = 0; i < FrameHeight; i++){
        //    for(int j=0; j<FrameWidth*2; j++){
        //        data.append(m_readData[i*FrameWidth*2+j]);
        //    }
            //m_standardOutput << QObject::tr("SECOND\n");
        //}

    minValue = 65535;
    maxValue = 0;
    gapAverage = 0;
    in = (unsigned char*)m_readData.data();
    //m_standardOutput << QObject::tr("THIRD\n");
    for(int i = 0; i < FrameWidth*FrameHeight*2; i++){
        //rawData.push_back(QString(QString(QChar(in[0])) + QString(QChar(in[1])) + QString(QChar(in[2])) + QString(QChar(in[3]))).toUShort(&converted, 10));
        rawData.push_back((unsigned short)(in[0] << 8 | in[1]));
        //m_standardOutput << QObject::tr("%1:%2 ").arg(rawData[i]).arg(i);
        in += 2;
    }
    //m_standardOutput << QObject::tr("FOURTH\n");
    
    for(int i = 80; i < FrameWidth*FrameHeight; i = i + FrameWidth){
        gapAverage += rawData[i] - rawData[i-1];
    }
    gapAverage = gapAverage / FrameHeight;
    for (int i = 0; i < FrameHeight; i ++){
        for (int j = 80; j < 160; j++){
            rawData[i*FrameWidth + j] = rawData[i*FrameWidth + j] - gapAverage;
        }
    }
    gapAverage = 0;
    for(int i = 160; i < FrameWidth*FrameHeight; i = i + FrameWidth){
        gapAverage += rawData[i] - rawData[i-1];
    }
    gapAverage = gapAverage / FrameHeight;
    for (int i = 0; i < FrameHeight; i ++){
        for (int j = 160; j < 240; j++){
            rawData[i*FrameWidth + j] = rawData[i*FrameWidth + j] - gapAverage;
        }
    }
    gapAverage = 0;
    for(int i = 240; i < FrameWidth*FrameHeight; i = i + FrameWidth){
        gapAverage += rawData[i] - rawData[i-1];
    }
    gapAverage = gapAverage / FrameHeight;
    for (int i = 0; i < FrameHeight; i ++){
        for (int j = 240; j < 320; j++){
            rawData[i*FrameWidth + j] = rawData[i*FrameWidth + j] - gapAverage;
        }
    }
    
        for(int i = 0; i < FrameWidth*FrameHeight; i++){
            if (rawData[i] < minValue) minValue = rawData[i];
            if (rawData[i] > maxValue) maxValue = rawData[i];
            //m_standardOutput << QObject::tr("%1 ").arg(rawData[i]);//:%2 ").arg(rawData[i]).arg(i);
        }
        //m_standardOutput << endl;
        emit updateImage(&rawData[0], minValue, maxValue);
        data.clear();
        rawData.clear();
        m_readData.clear();
        //exit(1);
        m_serialPort->write("a");
    }
}

void SerialPortReader::handleError(QSerialPort::SerialPortError serialPortError)
{
    if (serialPortError == QSerialPort::ReadError) {
        m_standardOutput << QObject::tr("An I/O error occurred while reading the data.") << endl; // from port %1, error: %2").arg(m_serialPort->portName()).arg(m_serialPort->errorString()) << endl;
        QCoreApplication::exit(1);
    }
}

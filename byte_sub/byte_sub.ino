#include <ros.h>
#include <std_msgs/Byte.h>

const byte RPWMPIN = 3;           // pin for pwm signal for right motor
const byte RDIRPIN = 12;          // pin for direction signal for right motor
const byte LPWMPIN = 11;          // pin for pwm signal for left motor
const byte LDIRPIN = 13;          // pin for direction signal for left motor
const unsigned long waitTime = 250;

unsigned long setTime = 0;
ros::NodeHandle  nh;

void setMotor(char m, int pwm){     // Standard set motor subroutine. Refactored from code provided on blackboard
  if (m == 'R') {
    if (pwm >= 0) {
      digitalWrite(RDIRPIN, LOW);
      analogWrite(RPWMPIN, pwm);
    } else {
      digitalWrite(RDIRPIN, HIGH);
      analogWrite(RPWMPIN, -pwm);
    }
  } else if (m == 'L') {
    if (pwm >= 0) {
      digitalWrite(LDIRPIN, HIGH);
      analogWrite(LPWMPIN, pwm);
    } else {
      digitalWrite(LDIRPIN, LOW);
      analogWrite(LPWMPIN, -pwm);
    }
  }
}

void messageCb( const std_msgs::Byte& msg){
  if ((msg.data & 0x01) == 1) {
    if ((msg.data & 0x10) == 16) {
      setMotor('L', 127);
      setMotor('R', 255);
    } else {
      setMotor('L', -255);
      setMotor('R', 255);
    }
  } else if ((msg.data & 0x02) == 2) {
    if ((msg.data & 0x10) == 16) {
      setMotor('L', 255);
      setMotor('R', 127);
    } else {
      setMotor('L', 255);
      setMotor('R', -255);
    }
  } else if ((msg.data & 0x10) == 16) {
    setMotor('L', 255);
    setMotor('R', 255);
  }
  if (msg.data != 0x00) {
    setTime = millis();
  }
}

ros::Subscriber<std_msgs::Byte> sub("robot_ctrl", &messageCb );

void setup()
{
  pinMode(13, OUTPUT);
  nh.initNode();
  nh.subscribe(sub);
}

void loop()
{
  nh.spinOnce();
  if (millis() - setTime > waitTime) {
    setMotor('L', 0);
    setMotor('R', 0);
  }
}


